import express, { Express } from "express";
import morgan from "morgan";
import * as routes from "./routes.js";

const app: Express = express();
const port = 80;

app.use(morgan("tiny"));

routes.register(app);

app.listen(port, () => {
  console.log(`Start server on port ${port}`);
});
