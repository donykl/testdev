import got from "got";

const defailtClientOptions = {
  timeout: {
    request: 10_000,
  },
};

export interface User {
  id: number;
  name: string;
  email: string;
  address: {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
  };
}

export interface Post {
  id: number;
  userId: number;
  title: string;
  body: string;
}

async function getUsers(): Promise<User[]> {
  console.log(`=> Fetching users from typicode`);

  const data: any = await got
    .get(`https://jsonplaceholder.typicode.com/users`, defailtClientOptions)
    .json();
  const users: User[] = data.map((user: any) => {
    return {
      id: user.id,
      name: user.name,
      email: user.email,
      address: {
        street: user.address.street,
        suite: user.address.suite,
        city: user.address.city,
        zipcode: user.address.zipcode,
      },
    };
  });

  return users;
}

async function getPosts(userId: number): Promise<Post[]> {
  console.log(`=> Fetching posts for user ${userId} from typicode`);

  const data: any = await got
    .get(`https://jsonplaceholder.typicode.com/posts?userId=${userId}`, defailtClientOptions)
    .json();
  const posts: Post[] = data.map((post: any) => {
    return {
      id: post.id,
      userId: post.userId,
      title: post.title,
      body: post.body,
    };
  });

  return posts;
}

export { getUsers, getPosts };
