import mysql from "mysql2/promise";

import * as db from "./db.js";
import * as typicode from "./typicode.js";

export interface PostResponse {
  total: number;
  totalFiltered: number;
  posts: typicode.Post[];
}

async function isUserExist(userId: number): Promise<boolean> {
  const [rows] = await db.pool.query<mysql.RowDataPacket[]>(
    `SELECT * FROM app_users WHERE id = ?`,
    [userId],
  );

  return rows.length > 0;
}

async function updatePosts(userId: number, posts: typicode.Post[]): Promise<typicode.Post[]> {
  console.log(`=> Putting posts len = ${posts.length} for user id = ${userId} into database`);

  const connection = await db.pool.getConnection();
  try {
    connection.beginTransaction();
    connection.query(`INSERT INTO app_users (id) VALUES (?)`, [userId]);

    const bulkInsert = posts.map((post) => {
      return [post.id, post.userId, post.title, post.body];
    });

    connection.query(`INSERT INTO app_posts (id, user_id, title, body) VALUES ?`, [bulkInsert]);
    connection.commit();
  } catch (err) {
    console.error(`=> Error while putting posts into database: ${err}`);
    connection.rollback();
  } finally {
    connection.release();
  }

  return posts;
}

export async function getTotalPosts(userId: number): Promise<number> {
  const [rows] = await db.pool.query<mysql.RowDataPacket[]>(
    `SELECT COUNT(*) AS count FROM app_posts WHERE user_id = ?`,
    [userId],
  );
  return rows[0].count;
}

export async function getTotalFiltered(userId: number, search: string): Promise<number> {
  const [rows] = await db.pool.query<mysql.RowDataPacket[]>(
    `SELECT COUNT(*) AS count FROM app_posts WHERE user_id = ? AND title LIKE ?`,
    [userId, `%${search}%`],
  );
  return rows[0].count;
}

export async function receivePosts(
  userId: number,
  after: number,
  limit: number,
  search: string,
): Promise<PostResponse> {
  const total = await getTotalPosts(userId);

  if (total === 0 && !(await isUserExist(userId))) {
    console.log(`=> User id=${userId} not found in database`);

    const fetchedPosts = await typicode.getPosts(userId);
    await updatePosts(userId, fetchedPosts);
    return receivePosts(userId, after, limit, search);
  }

  let rows: mysql.RowDataPacket[];
  let totalFiltered: number;

  if (search) {
    [rows] = await db.pool.query<mysql.RowDataPacket[]>(
      `
            SELECT * FROM app_posts 
            WHERE user_id = ? AND id > ? AND title LIKE ?
            ORDER BY id 
            LIMIT ?
        `,
      [userId, after, `%${search}%`, limit],
    );

    totalFiltered = await getTotalFiltered(userId, search);
  } else {
    [rows] = await db.pool.query<mysql.RowDataPacket[]>(
      `
            SELECT * FROM app_posts 
            WHERE user_id = ? AND id > ?
            ORDER BY id 
            LIMIT ?
         `,
      [userId, after, limit],
    );

    totalFiltered = total;
  }

  const posts: typicode.Post[] = rows.map((row: mysql.RowDataPacket) => {
    return {
      id: row.id,
      userId: row.user_id,
      title: row.title,
      body: row.body,
    };
  });

  return {
    total,
    totalFiltered,
    posts,
  };
}

export async function deletePost(userId: number, postId: number) {
  await db.pool.query(`DELETE FROM app_posts WHERE user_id = ? AND id = ?`, [userId, postId]);
}
