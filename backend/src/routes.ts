import * as express from "express";

import * as posts from "./posts.js";
import * as typicode from "./typicode.js";

const defailtLimit = 10;

async function handlerGetUsers(req: express.Request, res: express.Response) {
  const users = await typicode.getUsers();

  let skip = Number(req.query.skip) || 0;
  let limit = Number(req.query.limit) || defailtLimit;
  // Add boundary check
  skip = Math.max(skip, 0);
  limit = Math.max(Math.min(limit, defailtLimit), 0);

  // TODO: pagination

  res.json(users);
}

async function handlerGetPosts(req: express.Request, res: express.Response) {
  const userId = Number(req.params.userId);
  if (isNaN(userId)) {
    res.status(400).send("Invalid user id");
    return;
  }

  let after = Number(req.query.after) || 0;
  let limit = Number(req.query.limit) || defailtLimit;
  const search = req.query.search?.toString() || "";

  // Add boundary check
  after = Math.max(after, 0);
  limit = Math.max(Math.min(limit, defailtLimit), 0);

  const userPosts = await posts.receivePosts(userId, after, limit, search);
  res.json(userPosts);
}

async function handlerDeletePost(req: express.Request, res: express.Response) {
  const userId = Number(req.params.userId);
  if (isNaN(userId)) {
    res.status(400).send("Invalid user id");
    return;
  }

  const postId = Number(req.params.postId);
  if (isNaN(postId)) {
    res.status(400).send("Invalid post id");
    return;
  }

  await posts.deletePost(userId, postId);
  res.status(200).send();
}

export const register = (app: express.Application) => {
  app.get("/users", handlerGetUsers);
  app.get("/users/:userId/posts", handlerGetPosts);
  app.delete("/users/:userId/posts/:postId", handlerDeletePost);
};
