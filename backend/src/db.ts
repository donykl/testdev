import mysql from "mysql2/promise";


const access: mysql.PoolOptions = {
  host: process.env.DB_HOST,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
};

export const pool = await mysql.createPool(access);

await (async function initDB() {
  // TODO: Use migrations instead
  console.log("=> Initializing database");

  console.log("==> Create users table");
  await pool.query<mysql.ResultSetHeader[]>(`
    CREATE TABLE IF NOT EXISTS app_users (
        id int NOT NULL,
        
        CONSTRAINT user_id_pk PRIMARY KEY (id)
      ) COLLATE 'utf8_general_ci';
    `);

  console.log("==> Create posts table");
  await pool.query<mysql.ResultSetHeader[]>(`
    CREATE TABLE IF NOT EXISTS app_posts (
        id int NOT NULL,
        user_id int NOT NULL,
        title text COLLATE 'utf8mb4_bin' NOT NULL,
        body text COLLATE 'utf8mb4_bin' NOT NULL,
        
        CONSTRAINT posts_id_pk PRIMARY KEY (id),
        FOREIGN KEY (user_id) REFERENCES app_users (id)
      ) COLLATE 'utf8_general_ci';
      `);
})();
