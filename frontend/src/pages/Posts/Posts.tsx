import React, { useEffect } from "react";

import { useSearchParams, useNavigate } from "react-router-dom";

import List from "../../components/List/List";
import { usePostsAPI } from "../../contexts/postContext";
import styles from "./Posts.module.css";

export default function UsersPosts() {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const { posts, fetchPostsPerUser, deletePost, total,filteredTotal } = usePostsAPI();

  const handleFetchPost = (limit: number, after?: string, search?: string) => {
    const userId = searchParams.get("user");
    if (!userId) {
      navigate(`/`);
      return;
    }
    fetchPostsPerUser(Number(userId), limit, after, search);
  };

  const handleDeletePost = (postId: number) => {
    const userId = searchParams.get("user");
    if (!userId) {
      navigate(`/`);
      return;
    }

    deletePost(Number(userId), postId);
  };

  return (
    <div className={styles.posts}>
      <List 
        data={posts}
       fetchAction={handleFetchPost}
        deleteAction={handleDeletePost} 
        total={total}
        filteredTotal={filteredTotal}
        />
    </div>
  );
}
