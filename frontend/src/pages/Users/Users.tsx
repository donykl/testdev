import React from "react";
import AutoSortTable from "../../components/Table/AutoSortTablePhysic";

import { useUserAPI } from "../../contexts/userContext";
import { UserTableHeader } from "./UsersTableHeader";
import { User, UserAddress } from "../../helpers/types";

import { useNavigate } from "react-router-dom";
import styles from "./Users.module.css";

export default function Users() {
  const { users, fetchUsers } = useUserAPI();
  let navigate = useNavigate();

  const processAddressHelper = (userAddress: UserAddress): string => {
    return `${userAddress.street},${userAddress.suite},${userAddress.city},${userAddress.zipcode}`;
  };

  const formatData = (dataArr: any) => {
    const formattedArr: any = [];
    dataArr.forEach((element: User) => {
      formattedArr.push({
        id: element.id,
        name: element.name,
        email: element.email,
        address: processAddressHelper(element.address),
      });
    });
    return formattedArr;
  };

  const handleRowClick = (userId: number) => {
    return navigate(`/posts?user=${userId}`);
  };

  return (
    <div className={styles.pageWrapper}>
      <div className={styles.usersTableWrapper}>
        <AutoSortTable
          constColumnSizes={[50]}
          columns={UserTableHeader}
          data={users && formatData(users)}
          tableRequest={fetchUsers}
          rowClickAction={handleRowClick}
        />
      </div>
    </div>
  );
}

// TODO handle window resize
