// useMemo<MRT_ColumnDef<User>
const ADDRESS_WIDTH = "50px";
export const UserTableHeader = [
  {
    accessorKey: "name",
    header: "Name",
    enableColumnActions: false,
    enableSorting: true,
  },
  {
    accessorKey: "email",
    header: "Email",
    enableSorting: false,
    enableColumnActions: false,
  },
  {
    accessorKey: "address",
    header: "Address",
    enableSorting: false,
    enableColumnActions: false,
    size: 50,
    Header: ({ original }: any) => {
      return (
        <div
          style={{
            width: ADDRESS_WIDTH,
            height: "50px",
            textOverflow: "ellipsis",
            overflow: "hidden",
            whiteSpace: "nowrap",
          }}
        >
          Address
        </div>
      );
    },
    Cell: ({ row: { original } }: any) => {
      return (
        <div
          style={{ width: ADDRESS_WIDTH, height: "50px", display: "flex", alignItems: "center" }}
        >
          <span
            style={{
              textOverflow: "ellipsis",
              overflow: "hidden",
              whiteSpace: "nowrap",
            }}
          >
            {original.address}
          </span>
        </div>
      );
    },
  },
];
