export type UserAddress = {
  street: string;
  suite: string;
  city: string;
  zipcode: number;
};
export type User = {
  id: number;
  name: string;
  email: string;
  address: UserAddress;
};

export type Post = {
  id: number;
  title: string;
  body: string;
};
