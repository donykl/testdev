import React, { ReactElement, useEffect, useState, useRef, useLayoutEffect } from "react";

import { List } from "react-virtualized";
import { Post } from "../../helpers/types";

import styles from "./List.module.css";

type ListProps = {
  rowRendererProp: ({ key, index, isScrolling, isVisible, style }: any) => JSX.Element;
  rowCount: number;
};
interface ListPhysicProps extends ListProps {
  height: number;
  width: number;
}

export type RendererProps = {
  key: number;
  index: number;
  isScrolling: boolean;
  isVisible: boolean;
  style: any;
};

function ListPhysic({ width, height, rowCount,rowRendererProp }: ListPhysicProps) {

    return (
        <List
            width={width}
            height={height}
            rowCount={rowCount}
            rowHeight={150}
            rowRenderer={rowRendererProp}
        />
    )
}

//TODO migrate to fitDimensions HOC helper
export default function WrappedListPhysic({ ...props }: ListProps): ReactElement {
  const ref = useRef<HTMLDivElement>(null);

  const [height, setHeight] = useState<number>(0);
  const [width, setWidth] = useState<number>(0);

  useLayoutEffect(() => {
    if (!ref.current) return;
    setHeight(ref.current.offsetHeight);
    setWidth(ref.current.offsetWidth);
  }, []);

  return (
    <div className={styles.wrap} ref={ref}>
      <ListPhysic height={height} width={width} {...props} />
    </div>
  );
}
