import React, { useEffect, useState } from "react";

import styles from "./List.module.css";
import Search from "../Search/Search";
import WrappedListPhysic from "./ListPhysic";
import { RendererProps } from "./ListPhysic";
import { Post } from "../../helpers/types";

const LIMIT = 2;

export default function List({
  data,
  fetchAction,
  deleteAction,
  total,
  filteredTotal,
}: {
  data: Array<Post> | undefined;
    fetchAction: (limit: number, after?: string, search?: string) => void;
    deleteAction: (postId: number) => void;
  total: number;
  filteredTotal: number;
}) {
  const [listData, setListData] = useState<Array<Post>>([]);
  const [searchValueState, setSearchValueState] = useState<string>("");

  useEffect(() => {
    loadMore();
  }, []);

  useEffect(() => {
    if(data){
        setListData([...listData, ...data]);
    }
  }, [data]);

  const loadMore = (search = "", skipAfter?: boolean, limit = LIMIT, useStraightSearch?:boolean) => {
    let after = "";
    if (listData?.length > 0 && !skipAfter) {
      after = String(listData[listData.length - 1].id);
    }
    //TODO refactor
    const searchValue = useStraightSearch ? search : search || searchValueState;
    fetchAction(limit, after, searchValue);
  };

    const handleDelete = (postId: number) => {
        deleteAction(postId);
        
        // TODO very bad idea
        // no check for server responce
        // no err handling

        setTimeout(()=>{
            loadMore("",true,listData?.length);
            setListData([]);
        },1000)
        // setListData(data as Array<Post>);
    // setListData(data as Array<Post>);
    }

    //TODO migrate to separate component
    //TODO Handle More btn
    //TODO Handle Dynamic row height

    function rowRenderer({
        key, // Unique key within array of rows
        index, // Index of row within collection
        style, // Style object to be applied to row (to position it)
    }: RendererProps): JSX.Element {
        const post: Post = listData[index];
        return (
            <div className={styles.postItem} key={key} style={style}>
                <div className={styles.userBlock}>
                    <div className={styles.fakeUserIcon}></div>
                    User
                </div>
                <div className={styles.postContent}>
                    <div className={styles.postHeader}>
                        <div className={styles.postTitle}>
                            {post.title}
                        </div>
                        <div
                            className={styles.postAction}
                            onClick={() => { handleDelete(post.id) }}>
                        &#215;
                        </div>
                    </div>
                    <div className={styles.postBody}>
                        {post.body}
                    </div>
                </div>

            </div>
        );
    }

    //TODO Migrate to  separate Button component
    //TODO Migrate to  infinite scroll
    // based on last element visibility
    const renderLoadMoreBtn = () => {
        // console.log("listData",listData);
        if(!listData) return null;
        const amountOfrows = searchValueState ? filteredTotal : total
        const isVisible = listData.length < amountOfrows;
        // console.log("amountOfrows",amountOfrows,"searchValueState",searchValueState,filteredTotal , total);

        if(!isVisible) return null;
        return (
            <div className={styles.loadMorebtnWrapper}>
                <button
                    id={"loadMore"}
                    onClick={() => { loadMore() }}
                    className={styles.loadMorebtn}
                >
                    Load More
                </button>
            </div>

        )
    }

  const handleSearchChange = (searchValue: string) => {
    loadMore(searchValue, true, LIMIT, true);
    setSearchValueState(searchValue);
    setListData([]);
  };


  return (
    <div className={styles.list}>
      <Search changeValueAction={handleSearchChange} />
      <div className={styles.listWrapper}>
        <WrappedListPhysic
          rowCount={listData ? listData.length : 0}
          rowRendererProp={rowRenderer}
        />
      </div>

      {renderLoadMoreBtn()}
    </div>
  );
}
