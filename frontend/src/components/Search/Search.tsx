import React, { useState, useEffect } from "react";

import styles from "./Search.module.css";

let TIMER: NodeJS.Timeout;

export default function Search({
  changeValueAction,
}: {
  changeValueAction: (searchValue: string) => void;
}) {
  const [searchValue, setSearchValue] = useState("");

  //TODO migrate debounce 

  useEffect(() => {
    if (TIMER) {
      clearTimeout(TIMER);
    }
    console.log("USEEFF",searchValue)
    TIMER = setTimeout(() => {
    console.log("call",searchValue)
      changeValueAction(searchValue)
    }, 800)
  }, [searchValue])

  const handleValueChanged = (evt: any) => {
    console.log('evt.target.value)', evt.target.value);
    setSearchValue(evt.target.value);
  };
  return (
    <div className={styles.searchWrapper}>
      <input
        placeholder="Search"
        className={styles.searchInput}
        value={searchValue}
        onChange={handleValueChanged}
      />
    </div>
  );
}
