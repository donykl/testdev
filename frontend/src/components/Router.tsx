import React from "react";
import Users from "../pages/Users/Users";
import Posts from "../pages/Posts/Posts";

import { createBrowserRouter, RouterProvider } from "react-router-dom";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Users />,
  },
  {
    path: "posts",
    element: <Posts />,
  },
]);
export default function Router() {
  return <RouterProvider router={router} />;
}
