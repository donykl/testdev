import React, { ReactElement, useEffect, useState, useRef, useLayoutEffect } from "react";

import { MantineReactTable } from "mantine-react-table";

import Paginator from "./Paginator";

import styles from "./AutoSortTablePhysic.module.css";

export const DEFAUTL_LIMIT = 4;
export const DEFAUTL_SKIP = 0;

type AutoSortTableProps = {
  columns: any;
  tableRequest: () => Promise<any>;
  data: any;
  constColumnSizes?: Array<number>;
  rowClickAction?: (rowUnitId: number) => void;
};
interface AutoSortTablePhysicProps extends AutoSortTableProps {
  height: number;
  width: number;
}

const AutoSortPhysic = ({
  height,
  width,
  data,
  tableRequest,
  columns,
  constColumnSizes,
  rowClickAction,
}: AutoSortTablePhysicProps) => {
  const [requestLimitOffset, setrequestLimitOffset] = useState({
    limit: DEFAUTL_LIMIT,
    skip: DEFAUTL_SKIP,
  });
  // const [columnFilters, setColumnFilters] = useState<MRT_ColumnFiltersState>(
  //   []
  // );
  // const [globalFilter, setGlobalFilter] = useState<string>();
  // const [sorting, setSorting] = useState<MRT_SortingState>([]);
  // const [formattedData, setFormattedData] = useState();

  useEffect(() => {
    handleCall();
  }, []);

  const tableContainerRef = useRef<HTMLDivElement>(null);

  const handleCall = async (skip = DEFAUTL_SKIP, limit = DEFAUTL_LIMIT) => {
    tableRequest();
  };

  useEffect(() => {
    handleCall(requestLimitOffset.skip, requestLimitOffset.limit);
  }, [requestLimitOffset.skip]);

  const handleNextPage = () => {
    console.log("handleNextPage");
    setrequestLimitOffset((prev) => {
      return {
        limit: DEFAUTL_LIMIT,
        skip: prev.skip + DEFAUTL_LIMIT,
      };
    });
  };

  const handlePrevPage = () => {
    console.log("handlePrevPage");
    setrequestLimitOffset((prev) => {
      return {
        limit: DEFAUTL_LIMIT,
        skip: prev.skip - DEFAUTL_LIMIT,
      };
    });
  };

  //optionally access the underlying virtualizer instance
  // const rowVirtualizerInstanceRef = useRef<
  //   MRT_Virtualizer<HTMLDivElement, HTMLTableRowElement>
  // >(null);

  // useEffect(() => {
  //   //scroll to the top of the table when the sorting changes
  //   rowVirtualizerInstanceRef.current?.scrollToIndex(0);
  // }, [sorting]);

  const autoFitDynamicColumnsWidth = () => {
    let calcConsColumnsWidth = 0;

    if (constColumnSizes) {
      constColumnSizes?.forEach((el) => {
        calcConsColumnsWidth = calcConsColumnsWidth + el;
      });
    }
    const numOfAutoSizedColumns = constColumnSizes
      ? columns.length - constColumnSizes.length
      : columns.length;

    return (width - calcConsColumnsWidth) / numOfAutoSizedColumns;
  };

  const isFetching = false;
  const isLoading = false;
  if (!data) return <></>;
  return (
    <>
      <MantineReactTable
        enableColumnFilters={false}
        enableGlobalFilter={false}
        enableColumnDragging={false}
        enableTopToolbar={false}
        mantineTableBodyRowProps={({ row }) => ({
          onClick: (event) => {
            console.info(row);
            if (row?.original) {
              //TODO
              rowClickAction && rowClickAction(2);
            }
          },
          sx: {
            cursor: "pointer",
          },
        })}
        mantinePaginationProps={{
          rowsPerPageOptions: ["4"],
          // rowsPerPage:{4}

          // rowsPerPageOption
          // showRowsPerPage:false,
        }}
        columns={columns}
        data={data}
        enablePagination={true}
        // enableColumnActions={false}
        // enableRowVirtualization //optional, but recommended if it is likely going to be more than 100 rows
        // // onColumnFiltersChange={mergeColumnFilters}
        // onSortingChange={setSorting}
        state={{
          // columnFilters,
          isLoading,
          showAlertBanner: false,
          showProgressBars: isFetching,
          // sorting,
          columnSizing: {},
        }}
        defaultColumn={{
          maxSize: 900,
          minSize: 50,
          size: autoFitDynamicColumnsWidth(),
        }}
        layoutMode="grid"
        //Disables the default flex-grow behavior of the table cells
        mantineTableHeadCellProps={{
          sx: {
            flex: "0 0 auto",
          },
        }}
        mantineTableBodyCellProps={{
          sx: {
            flex: "0 0 auto",
          },
        }}
        initialState={{ pagination: { pageIndex: 0, pageSize: 4 } }}
        mantineTableContainerProps={{
          ref: tableContainerRef, //get access to the table container element
          height: `${height}px`,
          // sx: { height: `${height}px`, width: `${width}px` }, //give the table a max height
          sx: { width: `${width}px` }, //give the table a max height
        }}
        enableHiding={false}
        enableFullScreenToggle={false}
        enableBottomToolbar={true}
        // renderBottomToolbar={({ table }) => (
        //   <div className={styles.tableControlsWrapper}>
        //     <Paginator
        //     canNextPage={true}
        //     canPreviousPage={true}
        //     rowsAmount={10}
        //     previousPageAction={handleNextPage}
        //     nextPageAction={handlePrevPage}
        //     />
        //   </div>
        //   )}
      />
    </>
  );
};

//TODO migrate to fitDimensions HOC helper
export default function AutoSortTable({ ...props }: AutoSortTableProps): ReactElement {
  const ref = useRef<HTMLDivElement>(null);

  const [height, setHeight] = useState<number>(0);
  const [width, setWidth] = useState<number>(0);

  useLayoutEffect(() => {
    if (!ref.current) return;
    setHeight(ref.current.offsetHeight);
    setWidth(ref.current.offsetWidth);
  }, []);

  return (
    <div className={styles.wrap} ref={ref}>
      <AutoSortPhysic height={height} width={width} {...props} />
    </div>
  );
}
