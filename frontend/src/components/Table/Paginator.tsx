import React, { FC } from "react";
import styles from "./Paginator.module.css";

type PaginatorProps = {
  page?: any;
  rowsAmount: number;
  canPreviousPage: any;
  canNextPage: any;
  nextPageAction: any;
  previousPageAction: any;
};

const Paginator: FC<PaginatorProps> = ({
  page,
  canNextPage,
  canPreviousPage,
  nextPageAction,
  previousPageAction,
  rowsAmount,
}) => {
  return (
    <div className={styles.container}>
      <div>
        {/* {+page[0]?.id + 1} — {+page[page.length - 1]?.id + 1} of {rowsAmount} */}
        {rowsAmount}
      </div>
      <div className={styles.btnContainer}>
        <div
          id={"paginatorPrev"}
          onClick={previousPageAction}
          //   className={styles.btn}
        >
          prev
        </div>
        <div
          id={"paginatorNext"}
          onClick={nextPageAction}
          //   className={styles.btn}
        >
          next
        </div>
      </div>
    </div>
  );
};

export default Paginator;
