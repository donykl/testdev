import React from "react";
import "./App.css";
import Router from "./components/Router";
import { UserContextProvider } from "./contexts/userContext";
import { PostContextProvider } from "./contexts/postContext";

function App() {
  return (
    <UserContextProvider>
      <PostContextProvider>
        <Router />
      </PostContextProvider>
    </UserContextProvider>
  );
}

export default App;
