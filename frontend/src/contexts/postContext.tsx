import React, { useContext, useState, useEffect, createContext } from "react";
import axios from "axios";

import { Post } from "../helpers/types";


//TODO Migrate to unified CRUD context with loading and err states 

type IState = {
  posts?: Array<Post> | undefined;
  total: number;
  filteredTotal: number;
};

type IFunc = {
  fetchPostsPerUser: (id: number, limit: number, after?: string, search?: string) => Promise<void>;
  deletePost: (userId: number, postId: number) => Promise<void>;
};

type Props = IState & IFunc;

const APIContext = createContext({} as Props);

export function PostContextProvider({ children }: any) {
  const [posts, setPosts] = useState<Array<Post> | undefined>(undefined);
  const [total, setTotal] = useState<number>(0);
  const [filteredTotal, setFilteredTotal] = useState<number>(0);


  async function fetchPostsPerUser(id: number, limit: number, after?: string, search?: string) {
    const { data } = await axios.get(
      `/api/users/${id}/posts?after=${after}&limit=${limit}&search=${search}`
      // "https://jsonplaceholder.typicode.com/posts?userId=2",
    );
    setPosts(data.posts);
    setTotal(data.total);
    setFilteredTotal(data.totalFiltered);
// 
    // setPosts(data);
  }

  async function deletePost(userId: number, postId: number) {
    const { data } = await axios.delete(`/api/users/${userId}/posts/${postId}`);
    // console.log("DELETE DATA", data);
  }

  return (
    <APIContext.Provider
      value={{
        posts,
        fetchPostsPerUser,
        deletePost,
        total,
        filteredTotal,
      }}
    >
      {children}
    </APIContext.Provider>
  );
}

export function usePostsAPI() {
  const context = useContext(APIContext);
  if (context === undefined) {
    throw new Error("Context must be used within a Provider");
  }
  return context;
}
