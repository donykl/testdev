import React, { useContext, useState, useEffect, createContext } from "react";
import axios from "axios";

import { User } from "../helpers/types";

//TODO Migrate to unified CRUD context with loading and err states 

type IState = {
  users?: Array<User> | undefined;
};

type IFunc = {
  fetchUsers: () => Promise<void>;
};

type Props = IState & IFunc;

const APIContext = createContext({} as Props);

export function UserContextProvider({ children }: any) {
  const [users, setUsers] = useState<Array<User> | undefined>(undefined);

  async function fetchUsers() {
    const { data } = await axios.get(
      `http://localhost:80/api/users`
      // `https://jsonplaceholder.typicode.com/users`,
      // `/api/users`
    );
    setUsers(data);
  }

  return (
    <APIContext.Provider
      value={{
        users,
        fetchUsers,
      }}
    >
      {children}
    </APIContext.Provider>
  );
}

export function useUserAPI() {
  const context = useContext(APIContext);
  if (context === undefined) {
    throw new Error("Context must be used within a Provider");
  }
  return context;
}
